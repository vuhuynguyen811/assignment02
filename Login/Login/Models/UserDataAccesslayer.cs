﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using AutoMapper;
using System.Text;

namespace Login.Models
{
    public class UserDataAccesslayer
    {
        private readonly DBContext _context;
       
        public UserDataAccesslayer(DBContext context)
        {
            _context = context;
        }

        public string RegisterUser(User user)
        {
            string status = "fail";

            var userInDb = _context.Users.SingleOrDefault(u => u.UserId == user.UserId);

            string hash = Hash(user.Password);
            user.Password = hash;

            if (userInDb == null)
            {
                userInDb = new User();

                //userInDb.FirstName = user.FirstName;
                //userInDb.LastName = user.LastName;
                //userInDb.UserId = user.UserId;
                //userInDb.Password = user.Password;

                Mapper.Map<User, User>(user, userInDb);

                _context.Add(userInDb);
                _context.SaveChanges();

                return status = "success";
            }

            return status;
        }

        public string ValidateLogin(LoginViewModel user)
        {
            string status = "fail";

            var userInDb = _context.Users.SingleOrDefault(u => u.UserId == user.UserId);

            if (userInDb == null)
            {
                return status = "fail";
            }
            else
            {
                if (userInDb.Password == Hash(user.Password))
                    return status = "success";
            }

            return status;
        }

        public string Hash(string password)
        {
            var bytes = new UTF8Encoding().GetBytes(password);
            byte[] hashBytes;
            using (var algorithm = new System.Security.Cryptography.SHA512Managed())
            {
                hashBytes = algorithm.ComputeHash(bytes);
            }
            return Convert.ToBase64String(hashBytes);
        }
    }
}
