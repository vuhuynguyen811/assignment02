﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Login.Models
{
    [DataContract]
    public class LoginViewModel
    {
        [DataMember]
        [Required]
        [Display(Name = "User ID")]
        public string UserId { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public LoginViewModel(User user)
        {
            UserId = user.UserId;
            Password = user.Password;
        }

        public LoginViewModel(string userId, string passWord)
        {
            UserId = userId;
            Password = passWord;
        }

        public LoginViewModel()
        {
            UserId = "";
            Password = "";
        }
    }

   
}
