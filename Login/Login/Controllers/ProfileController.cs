﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Login.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Login.Controllers
{
    public class ProfileController : Controller
    {
		private readonly DBContext _context;

		public ProfileController(DBContext context)
		{
			_context = context;
		}

		[Authorize]
		public IActionResult Index()
		{
			return View();
		}

		[HttpGet]
		public IActionResult Detail()
		{
			var userId = this.User.Claims.Where(p => p.Type == ClaimTypes.Name).First().Value;

			var userInDb = _context.Users.Single(u => u.UserId == userId);

			return Json(userInDb);
		}
	}
}