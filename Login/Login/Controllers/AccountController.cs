﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Login.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace Login.Controllers
{
    public class AccountController : Controller 
    {
        private readonly DBContext _context;
        private UserDataAccesslayer userDataAccesslayer;
        
        public AccountController(DBContext context)
        {
            _context = context;
            userDataAccesslayer = new UserDataAccesslayer(_context);
        }

		[HttpGet]
        public IActionResult Login()
        {
            return View();
        }


        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync( CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction("Index", "Home");
        }


        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody]LoginViewModel inputUser)
        {					
            if (ModelState.IsValid)
            {
                string LoginStatus = userDataAccesslayer.ValidateLogin(inputUser);

                if (LoginStatus == "success")
                {
                    var claims = new List<Claim>
                    {
                         new Claim(ClaimTypes.Name, inputUser.UserId)
                     };

                    var claimsIdentity = new ClaimsIdentity(
                        claims, CookieAuthenticationDefaults.AuthenticationScheme);

                    var authProperties = new AuthenticationProperties
                    {
                        //AllowRefresh = <bool>,
                        // Refreshing the authentication session should be allowed.

                        //ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(10),
                        // The time at which the authentication ticket expires. A 
                        // value set here overrides the ExpireTimeSpan option of 
                        // CookieAuthenticationOptions set with AddCookie.

                        //IsPersistent = true,
                        // Whether the authentication session is persisted across 
                        // multiple requests. Required when setting the 
                        // ExpireTimeSpan option of CookieAuthenticationOptions 
                        // set with AddCookie. Also required when setting 
                        // ExpiresUtc.

                        //IssuedUtc = <DateTimeOffset>,
                        // The time at which the authentication ticket was issued.

                        //RedirectUri = <string>
                        // The full path or absolute URI to be used as an http 
                        // redirect response value.
                    };

                    await HttpContext.SignInAsync(
                        CookieAuthenticationDefaults.AuthenticationScheme,
                        new ClaimsPrincipal(claimsIdentity),
                        authProperties);

                       return Json(new { success = true, responseText = "Sucess" });                                     

                }
                else
                {
                    //TempData["UserLoginFailed"] = "Login Failed.Please enter correct credentials";
                    return Json(new { success = false, responseText = "Login Failed.Please enter correct credentials"});
                }
            }
            else
                return Json(new { success = false, responseText = "Login Failed.Please enter correct credentials" });

        }

        [HttpPost]
        public IActionResult Register(User user)
        {        
            if (ModelState.IsValid)
            {
               var registerStatus = userDataAccesslayer.RegisterUser(user);

                if (registerStatus == "success")
                {
                    ModelState.Clear();
                    TempData["Success"] = "Registration Successful!";
                    return View();
                }
                else
                {
                    TempData["Fail"] = "This User ID already exists. Registration Failed.";
                    return View();
                }
                
            }
            return View();
        }

    }
}